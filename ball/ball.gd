extends RigidBody2D

var game_started : bool = false

func _input(event):
	if event.is_action_pressed("init") and not game_started:
		linear_velocity = Vector2(50, -200)
		game_started = true
func _physics_process(delta):
	for body in get_colliding_bodies():
		if body.is_in_group("gr_blocks"):
			body.queue_free()
